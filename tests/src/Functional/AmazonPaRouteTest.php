<?php

namespace Drupal\Tests\amazon_pa\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests routes provided by amazon_pa module.
 *
 * @group amazon_pa
 */
class AmazonPaRouteTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'amazon_pa',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A simple user with 'access content' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $authUser;

  /**
   * Machine names of admin routes to test.
   *
   * @var string[]
   */
  protected $adminRoutes = [
    'amazon_pa.admin_settings',
    'amazon_pa.admin_settings_storage',
    'amazon_pa.admin_settings_test',
    'amazon_pa.admin_settings_database',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->authUser = $this->drupalCreateUser(['access content']);
  }

  /**
   * Check access on all admin routes.
   */
  public function testAdminRoutes(): void {
    $this->drupalLogin($this->authUser);

    foreach ($this->adminRoutes as $route_name) {
      $route = \Drupal::service('router.route_provider')
        ->getRouteByName($route_name);
      $is_admin = \Drupal::service('router.admin_context')
        ->isAdminRoute($route);
      $this->assertTrue($is_admin, "Admin route should be TRUE for $route_name");
      $this->drupalGet($route->getPath());
      $this->assertSession()->statusCodeEquals(403);
    }

    $this->drupalLogin($this->rootUser);
    foreach ($this->adminRoutes as $route_name) {
      $route = \Drupal::service('router.route_provider')
        ->getRouteByName($route_name);
      $this->drupalGet($route->getPath());
      $this->assertSession()->statusCodeEquals(200);
    }
  }

}
