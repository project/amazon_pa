<?php

namespace Drupal\amazon_pa\Form;

use Drupal\amazon_pa\Utils\AmazonPaUtils;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AmazonPaSettings extends ConfigFormBase {

  public function getEditableConfigNames() {
    return [
      'amazon_pa.settings',
    ];
  }

  public function getFormId() {
    return 'amazon_pa_admin_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('amazon_pa.settings');

    $utils = new AmazonPaUtils();
    $cache = $utils->amazon_pa_data_cache(FALSE);
    $options = [];

    foreach ($cache['locales'] as $locale => $data) {
      $options[$locale] = $data['name'];
    }

    $form['amazon_aws_access_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Amazon AWS Access Key ID'),
      '#description' => $this->t('You must sign up for an Amazon AWS account to use the Product Advertising Service.'),
      '#default_value' => $config->get('amazon_aws_access_key'),
      '#required' => TRUE,
    ];

    $form['amazon_aws_secret_access_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Amazon AWS Secret Access Key'),
      '#description' => $this->t('You must sign up for an Amazon AWS account to use the Product Advertising Service.'),
      '#default_value' => $config->get('amazon_aws_secret_access_key'),
      '#required' => TRUE,
    ];

    $form['amazon_default_locale'] = [
      '#type' => 'select',
      '#title' => $this->t('Default Amazon locale'),
      '#default_value' => $config->get('amazon_default_locale'),
      '#options' => $options,
      '#description' => $this->t('Amazon.com uses separate product databases and Ecommerce features in different locales; pricing and availability information, as well as product categorization, differs from one locale to the next. Be sure to select the default locale your site will be running in.'),
    ];

    $form['associate_setting'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Amazon referral settings'),
      '#description' => $this->t("Enter your associate ID for each locale you'd like to use on your site. Locales left blank will not be available.<br><strong>Note:</strong> By default the Drupal Association's ID will populate the field if it's available unless you have already specified your own or cleared the value."),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];

    foreach ($cache['locales'] as $locale => $data) {
      $form['associate_setting']['amazon_locale_' . $locale . '_associate_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t(':locale_name associate ID', [':locale_name' => $data['name']]),
        '#description' => $this->t('Enter your :locale_name associate ID to receive referral bonuses when shoppers purchase Amazon products via your site.', [':locale_name' => $data['name']]),
        '#default_value' => $config->get('amazon_locale_' . $locale . '_associate_id'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('amazon_pa.settings');

    // all locales manually...just ugly. There has to be a better way.
    $config
      ->set('amazon_aws_access_key', $form_state->getValue('amazon_aws_access_key'))
      ->set('amazon_aws_secret_access_key', $form_state->getValue('amazon_aws_secret_access_key'))
      ->set('amazon_default_locale', $form_state->getValue('amazon_default_locale'))
      ->set('amazon_locale_DE_associate_id', $form_state->getValue('amazon_locale_DE_associate_id'))
      ->set('amazon_locale_US_associate_id', $form_state->getValue('amazon_locale_US_associate_id'))
      ->set('amazon_locale_UK_associate_id', $form_state->getValue('amazon_locale_UK_associate_id'))
      ->set('amazon_locale_JP_associate_id', $form_state->getValue('amazon_locale_JP_associate_id'))
      ->set('amazon_locale_FR_associate_id', $form_state->getValue('amazon_locale_FR_associate_id'))
      ->set('amazon_locale_CA_associate_id', $form_state->getValue('amazon_locale_CA_associate_id'))
      ->set('amazon_locale_IT_associate_id', $form_state->getValue('amazon_locale_IT_associate_id'))
      ->set('amazon_locale_CN_associate_id', $form_state->getValue('amazon_locale_CN_associate_id'))
      ->set('amazon_locale_ES_associate_id', $form_state->getValue('amazon_locale_ES_associate_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
