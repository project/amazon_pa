<?php

namespace Drupal\amazon_pa\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AmazonPaStorage extends ConfigFormBase {

  public function getEditableConfigNames() {
    return [
      'amazon_pa.settings',
    ];
  }

  public function getFormId() {
    return 'amazon_pa_admin_storage_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('amazon_pa.settings');

    $period = [
      '3600'  => $this->t('1 hour'),
      '7200'  => $this->t('2 hours'),
      '14400' => $this->t('4 hours'),
      '21600' => $this->t('6 hours'),
      '43200' => $this->t('12 hours'),
      '86400' => $this->t('24 hours'),
    ];

    $form['details'] = [
      '#type'        => 'fieldset',
      '#title'      => 'Settings',
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];


    $form['details']['amazon_refresh_schedule'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Amazon refresh schedule'),
      '#description'   => $this->t('Cached information on Amazon items must be refreshed regularly to keep pricing and stock information up to date. Cron must be enabled for this function to work properly.'),
      '#default_value' => $config->get('details.amazon_refresh_schedule'),
      '#options'       => $period,
    ];
    $form['details']['amazon_refresh_cron_limit'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Limit'),
      '#description'   => $this->t('This will be the number of ASINS! processed each time cron runs.'),
      '#default_value' => $config->get('details.amazon_refresh_cron_limit'),
    ];
    $form['details']['amazon_core_data'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Store extended product data'),
      '#default_value' => $config->get('details.amazon_core_data'),
      '#options'       => [
        'creators' => $this->t('Book authors, film actors, etc.'),
        'images'   => $this->t('Product image links'),
      ],
    ];

    $form['details']['amazon_invalid_asin_alt'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Invalid ASIN URL'),
      '#description'   => $this->t('If an ASIN is INVALID we can not process it and beeing that display no valid affiliate link.
      So we can display a link we define here instead. You may want to use a link to the amazon Homepage with your affiliate ID in it.'),
      '#default_value' => $config->get('details.amazon_invalid_asin_alt'),
    ];


    $form['update'] = [
      '#title'      => 'Data update options',
      '#type'        => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];

    $form['update']['amazon_update_on_node_edit'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Update ASIN field data on node update(saving an existing node manually!).'),
      '#description'   => $this->t('Check this to update asin/api data on node save. This is especially usefuly if you mass save, like with VBO. It is also possible that you want to refresh the data if you save a node!'),
      '#default_value' => $config->get('update.amazon_update_on_node_edit'),
    ];

    $form['update']['amazon_update_on_node_view'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Update ASIN field data on node view.'),
      '#description'   => $this->t('Check this to update asin/api data when node is viewed. Useful to rebuild field data using a crawler or by user views.'),
      '#default_value' => $config->get('update.amazon_update_on_node_view'),
    ];

    $form['update']['amazon_update_on_node_hook_save'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Update ASIN field data on hook_node_update. (activate for VBO usage only)'),
      '#description'   => $this->t('Check this to update asin/api data on node save. This is especially usefuly if you mass save nodes to regenerate data, like done with VBO. <b>Deactivate it after!</b>'),
      '#default_value' => $config->get('update.amazon_update_on_node_hook_save'),
    ];


    $form['debug'] = [
      '#title'      => 'Debug and refresh options',
      '#type'        => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];
    $form['debug']['amazon_request_delay'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('DEBUG: Delay between requests'),
      '#description'   => $this->t('<b>This also slows down the page load by this amount! Only use for debug purposes</b>. This will fore a delay between two requests. Useful when you are NEW and get throtteled errors "too many requests", see logfiles.'),
      '#default_value' => $config->get('debug.amazon_request_delay'),
    ];
    $form['debug']['amazon_request_amount'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('DEBUG: Requests sent'),
      '#description'   => $this->t('Counting requests for debug purpose.'),
      '#default_value' => $config->get('debug.amazon_request_amount'),
    ];
    $form['debug']['amazon_request_amount_checkbox'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Activate request counter'),
      '#description'   => $this->t('Counting requests for debug purpose.'),
      '#default_value' => $config->get('debug.amazon_request_amount_checkbox'),
    ];

    $form['debug']['amazon_request_enabled'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('API requests enabled'),
      '#description'   => $this->t('Uncheck this to completely prevent any web API requests. No new data will be fetched, but ASINs that have already been queried can still be used.'),
      '#default_value' => $config->get('debug.amazon_request_enabled'),
    ];

    $form['debug']['amazon_only_nodes'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Trigger API request only on a node. Meaning node/x'),
      '#description'   => $this->t('Debug option.'),
      '#default_value' => $config->get('debug.amazon_only_nodes'),
    ];

    $form['tokens'] = [
      '#type'        => 'fieldset',
      '#title'      => 'Tokens',
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];
    $form['tokens']['amazon_token_request_delay'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Delay for token requests(will raise page load times with asins > 10)'),
      '#description'   => $this->t('Amazon has a throtteling limit! A request can have a maximum of 10 ASINS. So if you overuse ASINS with tokens you will get a throtteling error if you use > 10 different ones. This delay can fix that!<b> Remember this will increase non cached loading times by that amount too. USER WITH CAUTION!</b>'),
      '#default_value' => $config->get('tokens.amazon_token_request_delay'),
    ];


    return parent::buildForm($form, $form_state);

  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('amazon_pa.settings');

    $config
      ->set(
        'details.amazon_refresh_schedule',
        $form_state->getValue('amazon_refresh_schedule')
      )
      ->set(
        'details.amazon_invalid_asin_alt',
        $form_state->getValue('amazon_invalid_asin_alt')
      )
      ->set(
        'details.amazon_refresh_cron_limit',
        $form_state->getValue('amazon_refresh_cron_limit')
      )
      ->set(
        'debug.amazon_request_delay',
        $form_state->getValue('amazon_request_delay')
      )
      ->set(
        'debug.amazon_request_amount',
        $form_state->getValue('amazon_request_amount')
      )
      ->set(
        'debug.amazon_request_amount_checkbox',
        $form_state->getValue('amazon_request_amount_checkbox')
      )
      ->set(
        'debug.amazon_request_enabled',
        (bool) $form_state->getValue('amazon_request_enabled')
      )
      ->set(
        'debug.amazon_only_nodes',
        $form_state->getValue('amazon_only_nodes')
      )
      ->set(
        'details.amazon_core_data',
        $form_state->getValue('amazon_core_data')
      )
      ->set(
        'update.amazon_update_on_node_edit',
        $form_state->getValue('amazon_update_on_node_edit')
      )
      ->set(
        'update.amazon_update_on_node_view',
        $form_state->getValue('amazon_update_on_node_view')
      )
      ->set(
        'update.amazon_update_on_node_hook_save',
        $form_state->getValue('amazon_update_on_node_hook_save')
      )
      ->set(
        'tokens.amazon_token_request_delay',
        $form_state->getValue('amazon_token_request_delay')
      )
      ->save();

    parent::submitForm($form, $form_state);
  }

}


