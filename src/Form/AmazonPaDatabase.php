<?php

namespace Drupal\amazon_pa\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AmazonPaDatabase extends ConfigFormBase {

  public function getEditableConfigNames() {
    return [
      'amazon_pa.settings',
    ];
  }

  public function getFormId() {
    return 'amazon_pa_admin_database_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['database_info'] = [
    '#markup' => '<p><b>Invalid ASINS should be removed to avoid throtteling issues. This pages removes them from the "items_table". Remember to change the nodes using this ASINS in a field or token too, or they will be regenerated!</b></p>',
    ];

    // delete a single asin
    $form['asin_delete'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delete a single Amazon ASIN from database'),
      '#description' => $this->t('The ASIN of a product listed on Amazon.'),
      '#required' => FALSE,
    ];
    $form['asin_delete_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete ASIN'),
      '#submit' => ['::amazon_pa_database_form_submit'],
      '#validate' => ['::amazon_pa_database_form_validate'],
    ];

    // delete all invalid asins from amazon_items table
    $form['invalid_asins'] = [
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['invalid_asins']['delete_invalid_asins'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete ALL invalid ASINs from database'),
      '#submit' => ['::amazon_pa_delete_invalid_all_asins_submit'],
      '#validate' => ['::amazon_pa_delete_invalid_all_asins_validate'],
    ];

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  public function amazon_pa_database_form_validate(array &$form, FormStateInterface $form_state) {
    //todo validate
  }
  public function amazon_pa_database_form_submit(array &$form, FormStateInterface $form_state) {
    $connection = \Drupal::service('database');
    $asin = $form_state->getValue('asin_delete');

    $connection->delete('amazon_item')
      ->condition('asin', $asin, '=')
      ->execute();
  }

  public function amazon_pa_delete_invalid_all_asins_validate(array &$form, FormStateInterface $form_state) {
    //todo validate
  }
  public function amazon_pa_delete_invalid_all_asins_submit(array &$form, FormStateInterface $form_state) {
    $connection = \Drupal::service('database');
    $connection->delete('amazon_item')
      ->condition('invalid_asin', 1, '=')
      ->execute();
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }

}
