<?php

namespace Drupal\amazon_pa\Form;

use Drupal\amazon_pa\Utils\AmazonPaUtils;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

class AmazonPaTest extends ConfigFormBase {

  public function getEditableConfigNames() {
    return [
      'amazon_pa.settings',
    ];
  }

  public function getFormId() {
    return 'amazon_pa_admin_test_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('amazon_pa.test_settings');
    $utils = new AmazonPaUtils();
    $cache = $utils->amazon_pa_data_cache(FALSE);

    $options = [];
    foreach ($cache['locales'] as $locale => $data) {
      // $this->config('amazon_pa.settings')->get(('amazon_locale_'. $locale .'_associate_id'));
      // only show if we have configured the associate tags
      if ($this->config('amazon_pa.settings')->get(('amazon_locale_' . $locale . '_associate_id'))) {
        $options[$locale] = $data['name']->__toString();
      }
    }

    $form['asin'] = [
      '#type' => 'textfield',
      '#title' => t('Amazon Product ID'),
      '#description' => t('The ASIN of a product listed on Amazon.'),
      '#required' => TRUE,
    ];
    $form['locale'] = [
      '#type' => 'select',
      '#title' => t('Locale'),
      '#options' => $options,
      '#default_value' => $config->get('amazon_default_locale'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Look Up Product'),
    ];

    $asin = $form_state->getValue('asin');
    $amazon_item = $form_state->getValue('amazon_item');

    if (isset($asin)) {
      $form['item'] = [
        '#type' => 'fieldset',
        '#title' => t('Result'),
        '#collapsible' => FALSE,
      ];

      $item_setting = [
        '#theme' => 'amazon_item_test',
        '#item' => $amazon_item,
        // '#style' => 'test',
      ];

      $form['item']['display'] = [
        '#markup' => \Drupal::service('renderer')->render($item_setting),
        '#weight' => 9,
      ];

      $form['item']['details'] = [
        '#type' => 'fieldset',
        '#title' => t('Details'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#weight' => 10,
      ];

      $form['item']['details']['data'] = [
        '#markup' => '<pre><small>' . print_r($amazon_item, TRUE) . '</small></pre>',
      ];
    }
    return parent::buildForm($form, $form_state);

  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $asin = $form_state->getValue('asin');
    $locale = $form_state->getValue('locale');

    // we only look up ONE item!
    $items = amazon_pa_item_lookup_from_web([$asin], $locale);

    if (!empty($items) && is_array($items)) {
      // $form_state['amazon_item'] = array_pop($items);
      $form_state->setValue('amazon_item', array_pop($items));
    }
    else {
      $url = Url::fromUserInput('admin/reports/dblog');

      $form_state->setErrorByName('asin', t("%input is no valid ASIN. Please check the !link for messages.",
      ['%input' => $asin, '!link' => Link::fromTextAndUrl(t("error log"), $url)]));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $item = $form_state->getValue('amazon_item');
    $asin = $form_state->getValue('asin');
    $locale = $form_state->getValue('locale');

    amazon_pa_item_delete($asin, $locale);
    amazon_pa_item_insert($item);
    $form_state->setRebuild();

    parent::submitForm($form, $form_state);
  }

}
