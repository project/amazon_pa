<?php

namespace Drupal\amazon_pa\Utils;

class AmazonPaUtils {

  /**
   * Get amazon locale
   *
   * @return array
   */
  private function amazon_pa_default_locales() {
    // Load default locales from includes/amazon_pa.locales.inc
    require_once \Drupal::service('extension.list.module')->getPath('amazon_pa')
      . '/includes/amazon_pa.locales.inc';
    $locales = _amazon_pa_load_locales();

    return $locales;
  }

  /**
   * Misc. helper functions for managing the wide array of Amazon
   * data bitsies.
   * Also contains neccessary api data see amazon_pa.locales.inc
   */
  public function amazon_pa_data_cache($reset = FALSE) {
    $data = &drupal_static(__FUNCTION__);

    $cid = 'amazon_pa:' . \Drupal::languageManager()->getCurrentLanguage()->getId();
    // $cid = 'amazon_pa:cache';

    if (!isset($data) || $reset) {
      if (!$reset && ($cache = \Drupal::cache()->get($cid))
        && !empty($cache->data)
      ) {
        $data = $cache->data;
      }
      else {
        $data = [];
        $data['locales'] = $this->amazon_pa_default_locales();
        \Drupal::cache()->set($cid, $data);
      }
    }

    return $data;

  }

}
