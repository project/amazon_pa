<?php

namespace Drupal\amazon_pa_filter\Plugin\Filter;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
use Drupal\Core\Form\FormStateInterface;

/**
 * @Filter(
 *   id = "amazon",
 *   title = @Translation("Amazon filter"),
 *   description = @Translation("Provides access to many types of Amazon data. Simplest usage: [amazon:ASIN:inline], for example [amazon:0596515804:inline]."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class AmazonFilter extends FilterBase {

  /**
   * process the filter
   *
   * @param string $text
   * @param string $langcode
   *
   * @return \Drupal\filter\FilterProcessResult
   *
   * This is not a simple one. The filter has to check a lot of stuff
   * Amazon API also returns nothing at all if you input invalid ASINS
   * Over time an ASIN can become invalid!
   * Thats why we have different checks in the whole code, not only here.
   */
  public function process($text, $langcode) {

    // make this global and use it as a kind of cache for one run. On the first run we generate all ASIN data and reuse it for every other run!
    // that is because the filter will run for every ASIN. Meaning 5 tokens found = 5 runs. Would reusult in API hammering and thats very ineffective and leads to throttle bans.
    // we then check if the asin data is already present because ASINS can be used multiple times on a page
    global $amz_items;
    global $asin_list;

    //regex to match amazon tokens
    $pattern = "@\[amazon(?: |:|%20)+(.*?)(?:(?: |:|%20)(.*?))?\]@";
    $matches = [];

    if (preg_match_all($pattern, $text, $matches)) {

      $tokens = $matches[0];
      $asins = $matches[1];
      $actions = $matches[2];
      $replace = [];

      // MULTIPLE ASINS in one request, 10 allowed. Solo requests triggers throtteling…VERY fast if multiple tokens used on one page
      // if someone overuses tokens on a page..then we need to raise the random sleep
      // that may be a problem in php timeout and is not really great
      // no other way if you are new and only get 1 request per second.
      $asin_count = 0;

      // skip this if we have the asins cached already!
      // else...$amz_items has already cached content
      //only do that if we have all items that are requested already cached!
      //I had a case where a textfilter of another node(1) was run first cache was build
      //and then the 2nd ran and used the cache of node 1(dont know yet why)
      //But that leaded to completely no processing at all because of the wrong cache!
      //dev note to myself. That happened on my wlan page
      //in tehory it would be possible to display 1-x nodes bodys on a page. so that must be teken into consideration for the cache!
      $cache_valid = true;
      foreach ($asins as $tkey => $tval) {
        if(!isset($amz_items[$tval])) {
          //cache invalid, purge for thsi run
          $amz_items = array();
        }
      }

      if (empty($amz_items)) {
        $amz_items = [];
        $asin_list = [];

        // A: all asins to an array, max. 10 as api allows max. of 10, then request to amazonapi
        // B: if more than 10, request then merge, then loop again until finish

        // Logic for processing ASINs
        // 1 create all data
        // 2 split into array chunks with a maximum of 10 items (api max is 10)
        // 3 loop these, get api data
        // 4 build final array

        // extract all ASINs from tokens
        foreach ($tokens as $key => $token) {
          //grouped asins (group token) like xyz|def|abc
          //extract these and process like solo asins
          if (strpos($asins[$key], '|') !== FALSE) {
            $group_indices[$key] = $key;
            $asins_grouped[$key] = explode('|', $asins[$key]);
            foreach ($asins_grouped[$key] as $gasin) {
              $asin_list[] = $gasin;
            }
          }
          // single asisn
          else {
            $asin_list[] = $asins[$key];
          }
        }

        // now split array to chunks of 10 and do the api requests
        // remove duplicate asins first to reduce api requests
        $asin_chunks = array_chunk(array_unique($asin_list), 10);

        // Amazon API does only allow 10 in one request!
        // using a lot of asins in tokes so may lead to throtteling/temporary bans!
        // Sleep time possible
        $config = \Drupal::config('amazon_pa.settings');
        $sleep_time = (int) $config->get('tokens.amazon_token_request_delay');
        $count = count($asin_chunks);

        //what is returned when we send invalid asins here? only valid results are returned
        //amazon discards INVALID ASINS but we have them in our process array. remove them for the process
        //later we check them and replace them with something else? amazon homepage instead of deadlinks?
        foreach ($asin_chunks as $key => $group) {
          $amz_items = amazon_pa_item_lookup($group, FALSE, NULL)
            + $amz_items;
          //more than 10 asins, so sleep after the request, but not on the last request
          if ($count > 1 && ($key != $count - 1)) {
            sleep($sleep_time);
          }
        }
      }

      //DO NOT process invalid asins.
      // should be fixed manually. no use in processing invalid data
      // IMPORTANT: Build back a clean strutured array from $matches and api
      // results for template processing
      // add back duplicate ASINS for better processing
      $amazon_data = array();

      //@todo problems with the cache on second run

      foreach ($actions as $mkey => $action) {
        $action_asin = $asins[$mkey];
        // single asin
        if ($action != 'group') {
          //there may be no result for this asin because it may be invalid or is already invalid  check
          if(isset($amz_items[$action_asin]) && $amz_items[$action_asin]['invalid_asin'] != 1) {
              $amazon_data[$mkey]['action'] = $action;
              $amazon_data[$mkey]['amazon_data'] = $amz_items[$action_asin];
          }
          //should we output a default element on invalid asins??
          else {

          }
        }
        // asin group
        else {
          $asins_group = explode('|', $action_asin);
          foreach ($asins_group as $key => $asin) {
            if(isset($amz_items[$asin]) && $amz_items[$asin]['invalid_asin'] != 1) {
              $amazon_data[$mkey]['action'] = $action;
              $amazon_data[$mkey]['amazon_data'][] = $amz_items[$asin];
            }
            //should we output a default element on invalid asins??
            else {

            }
          }
        }
      }

      // get all "actions" (detail, thumbnail, fill and so on) we later run for the replacement of tokens
      $processed_asins = [];

      foreach ($amazon_data as $key => $item_data) {
        $action = $item_data['action'];
        $item = $item_data['amazon_data'];

        // different actions use different templates see template folder
        switch ($action) {
          case "group":
            // group has multiple items
            $items = $item;
            $render_data = [
              '#theme' => 'amazon_asin_group',
              '#item' => $items,
              '#attached' => [
                'library' => [
                  'amazon_pa/amazon_pa',
                ],
              ],
            ];
            $render_ = \Drupal::service('renderer')->render($render_data);
            // here again is no subarray like in other cases so no [0]
            $replace[] = trim($render_->__toString());
            foreach ($items as $itemdata) {
              $processed_asins[] = $itemdata['asin'];
            }
            break;

          // simple button
          case 'button':
            $render_data = [
              '#theme' => 'amazon_item_button',
              '#item' => $item,
              '#attached' => [
                'library' => [
                  'amazon_pa/amazon_pa',
                ],
              ],
            ];
            $render_ = \Drupal::service('renderer')->render($render_data);
            // here again is no subarray like in other cases so no [0]
            $replace[] = trim($render_->__toString());
            $processed_asins[] = $item['asin'];
            break;

          // sales button
          case 'sbutton':
            $render_data = [
              '#theme' => 'amazon_item_sbutton',
              '#item' => $item,
              '#attached' => [
                'library' => [
                  'amazon_pa/amazon_pa',
                ],
              ],
            ];
            $render_ = \Drupal::service('renderer')->render($render_data);
            // here again is no subarray like in other cases so no [0]
            $replace[] = trim($render_->__toString());
            $processed_asins[] = $item['asin'];
            break;

          // widget mimicing the amazon image+text iframe widget
          case 'amzwidget':
            $render_data = [
              '#theme' => 'amazon_widget',
              '#item' => $item,
              '#attached' => [
                'library' => [
                  'amazon_pa/amazon_pa',
                ],
              ],
            ];
            $render_ = \Drupal::service('renderer')->render($render_data);
            // here again is no subarray like in other cases so no [0]
            $replace[] = trim($render_->__toString());
            $processed_asins[] = $item['asin'];
            break;

          // If someone skips the action parameter..
          case "":
          case 'inline':
            $render_data = [
              '#theme' => 'amazon_pa_item',
              '#item' => $item,
              '#attached' => [
                'library' => [
                  'amazon_pa/amazon_pa',
                ],
              ],
            ];
            $render_ = \Drupal::service('renderer')->render($render_data);
            // here again is no subarray like in other cases so no [0]
            $replace[] = trim($render_->__toString());
            $processed_asins[] = $item['asin'];
            break;

          // Full is a synonym of 'details'.
          case 'full':
          case 'details':
            $render_data = [
              '#theme' => 'amazon_details',
              '#item' => $item,
              '#attached' => [
                'library' => [
                  'amazon_pa/amazon_pa',
                ],
              ],
            ];
            $render_ = \Drupal::service('renderer')->render($render_data);
            // here again is no subarray like in other cases so no [0]
            $replace[] = trim($render_->__toString());
            $processed_asins[] = $item['asin'];
            break;

          // Handle themeable cases, like thumbnail.
          case 'thumbnail':
            $render_data = [
              '#theme' => 'amazon_item_thumbnail_medium',
              '#item' => $item,
              '#attached' => [
                'library' => [
                  'amazon_pa/amazon_pa',
                ],
              ],
            ];
            $render_ = \Drupal::service('renderer')->render($render_data);
            $replace[] = trim($render_[0]->__toString());
            $processed_asins[] = $item['asin'];
            break;

          default:
            // Allow to use anything found in the item array.
            // Like title, asin, detailpageurl $action contains the variable name
            $render_data = [
              '#theme' => 'amazon_detail',
              '#item' => $item,
              '#detail' => $action,
              '#attached' => [
                'library' => [
                  'amazon_pa/amazon_pa',
                ],
              ],

            ];
            $render_ = \Drupal::service('renderer')->render($render_data);
            // here again is no subarray like in other cases so no [0]
            $replace[] = trim($render_->__toString());

            //@todo debug
            if(is_null($item['asin'])){
              $stop=1;
            }

            $processed_asins[] = $item['asin'];
            break;
        }
      }

      //asins where we have no results or invalid must be marked
      //on invalid asins we output the link specified in the options
      //this will only happen if an asin was VALID on input but became invalid over time
      $config = \Drupal::config('amazon_pa.settings');
      $link = $config->get('details.amazon_invalid_asin_alt');
      foreach ($asin_list as $asin) {
        if (!in_array($asin, $processed_asins)) {
          $replace[] = $link;
        }
      }

      $text = str_replace($tokens, $replace, $text);
    }

    return new FilterProcessResult($text);
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // not needed, would show options on the filter page/textformat where you activate it
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return t('Get Amazon product data using [amazon:ASIN:selector], for example, [amazon:0399155341:thumbnail],
    [amazon:0399155341:full], or [amazon:0399155341:inline].<br>
    <p>Grouping is possible [amazon:0399155341|0399155341|0399155341:group]</p>
    <p>In addition, you can grab various data items from the item description using selectors like:</p>
    <ul>
      <li>author</li>
      <li>title</li>
      <li>asin</li>
      <li>mpn</li>
      <li>ean</li>
      <li>detailpageurl</li>
      <li>salesrank</li>
      <li>manufacturer</li>
      <li>brand</li>
      <li>binding</li>
      <li>listpriceamount</li>
      <li>listpricecurrencycode</li>
      <li>listpriceformattedprice</li>
      <li>lowestpriceamount</li>
      <li>lowestpricecurrencycode</li>
      <li>lowestpriceformattedprice</li>
      <li>amazonpriceamount</li>
      <li>amazonpricecurrencycode</li>
      <li>amazonpriceformattedprice</li>
      <li>productgroup</li>
      <li>producttypename</li>
      <li>invalid_asin</li>
      <li>publicationyear</li>
      <li>releaseyear</li>
      <li>type</li>
      <li>publicationyear</li>
      <li>smallimage</li>
      <li>smallimageurl</li>
      <li>smallimageheight</li>
      <li>smallimagewidth</li>
      <li>mediumimage</li>
      <li>mediumimageurl</li>
      <li>mediumimageheight</li>
      <li>mediumimagewidth</li>
      <li>largeimage</li>
      <li>largeimageurl</li>
      <li>largeimageheight</li>
      <li>largeimagewidth</li>
    </ul>
    <p>
    For example, [amazon:0596515804:title] will provide the title of the item, and
    [amazon:0596515804:largeimage] will be replaced with an img tag giving the large image.
    </p>
    <p>
    <b>NOTE: Spaces can also be used instead of colons between the elements of the filter tag, but should be considered a deprecated form of notation.</b>
    </p>
    ');
    }
    else {
      return t('Link to Amazon products with: [amazon:product_id:inline|full|thumbnail|datadescriptor]. Example: [amazon:1590597559:thumbnail] or [amazon:1590597559:author]. Details are <a href="http://drupal.org/node/595464#filters" target="_blank">on the Amazon module handbook page</a>.');
    }
  }


}
