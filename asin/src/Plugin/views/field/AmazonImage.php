<?php

namespace Drupal\asin\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Link;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\Core\Url;

/**
 * @file
 * Definition of Drupal\asin\Plugin\views\field\AmazonImage
 */

/**
 * Field handler for amazon image.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("views_handler_field_amazon_image")
 */
class AmazonImage extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.

    $this->ensureMyTable();
    // add the fields
    $this->addAdditionalFields();
  }

  /**
   * Override ensureMyTable
   *
   * We do our own LEFT JOIN here to get image sizes
   *
   * @return string
   */
  public function ensureMyTable() {
    if (empty($this->table_alias)) {
      // we only want images that match our size selection in the option on the UI!
      $definition = [
        'table'      => 'amazon_item_image',
        'field'      => 'asin',
        'left_table' => 'amazon_item',
        'left_field' => 'asin',
        'operator'   => '=',
        'extra'      => [
          0 => [
            'field' => 'size',
            'value'      => $this->options['image_size'],
            'numeric' => FALSE,
          ],
        ],
      ];
      $join = \Drupal::service('plugin.manager.views.join')->createInstance('standard', $definition);
      $this->tableAlias = $this->query->ensureTable($this->table, $this->relationship, $join);
    }
  }

  /**
   * Define the available options
   *
   * @return array
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['image_size'] = ['default' => 'smallimage'];
    $options['link_format'] = ['default' => 'amazon'];
    $options['presentation_format'] = ['default' => 'markup'];
    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    $form['image_size'] = [
      '#title'         => t('Image size'),
      '#type'          => 'select',
      '#options'       => [
        'smallimage'  => t('Small'),
        'mediumimage' => t("Medium"),
        'largeimage'  => t("Large"),
      ],
      '#default_value' => $this->options['image_size'],
    ];

    $form['link_format'] = [
      '#title'         => t('Link behavior'),
      '#type'          => 'radios',
      '#options'       => [
        'plain'  => t('No link'),
        'amazon' => t("A link to the product's Amazon page"),
      ],
      '#default_value' => $this->options['link_format'],
    ];

    $form['presentation_format'] = [
      '#title'         => t('Presentation format'),
      '#type'          => 'select',
      '#options'       => [
        'markup'    => t('Image as HTML'),
        'plain_url' => t('Image URL as text'),
      ],
      '#default_value' => $this->options['presentation_format'],
    ];

    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * Override init function to provide generic option to link to node.
   *
   * @param \Drupal\views\ViewExecutable $view
   * @param \Drupal\views\Plugin\views\display\DisplayPluginBase $display
   * @param array|null $options
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    // this results in multiple results without conitional filtering
    //    $this->additional_fields = [];
    //    if (!empty($this->definition['additional fields'])) {
    //      $this->additional_fields = $this->definition['additional fields'];
    //    }
    //
    //    //add fields we add in the settings, we need that for render()
    //    $this->additional_fields['url'] = array(
    //      'table' => 'amazon_item_image',
    //      'field' => 'url',
    //    );
    //    $this->additional_fields['asin'] = array(
    //      'table' => 'amazon_item_image',
    //      'field' => 'asin',
    //    );
    //    $this->additional_fields['height'] = array(
    //      'table' => 'amazon_item_image',
    //      'field' => 'height',
    //    );
    //    $this->additional_fields['width'] = array(
    //      'table' => 'amazon_item_image',
    //      'field' => 'width',
    //    );
    //    $this->additional_fields['detailpageurl'] = array(
    //      'table' => 'amazon_item',
    //      'field' => 'detailpageurl',
    //    );

    // do this not the above!
    // we overwrite ensureMyTable to get the fields
    // this forces the fields to be in the SELECT
    $this->additional_fields['height'] = 'height';
    $this->additional_fields['width'] = 'width';
    $this->additional_fields['detailpageurl'] = [
      'table' => 'amazon_item',
      'field' => 'detailpageurl',
      'value' => 'amazon_item_detailpageurl',
    ];
    $this->additional_fields['asin'] = 'asin';
    $this->additional_fields['url'] = 'url';

  }

  /**
   * @{inheritdoc}
   *
   * @param \Drupal\views\ResultRow $values
   *
   * @return \Drupal\Core\Link|null
   */
  public function render(ResultRow $values) {
    $url = $this->getValue($values, 'url');
    $asin = $this->getValue($values, 'asin');
    $detailpageurl = $this->getValue($values, 'detailpageurl');
    $width = $this->getValue($values, 'width');
    $height = $this->getValue($values, 'height');

    // We may not have a URL. It's not guaranteed that Amazon will give us one.
    if (empty($url)) {
      return;
    }
    // Choose presentation style
    if ($this->options['presentation_format'] == 'markup') {
      // $image = theme('image', ['path' => $url, 'alt' => NULL, 'title' => NULL, 'attributes' => $attributes, 'getsize' => FALSE]);

      $image_ = [
        '#theme'    => 'image',
        '#uri'       => $url,
        '#alt'        => NULL,
        '#title'      => NULL,
        '#width'      => $width,
        '#height'      => $height,
      ];
      // $variables[$key] = \Drupal::service('renderer')->render($image_);
      $image = \Drupal::service('renderer')->render($image_);

    }
    else {
      $image = $url;
    }

    switch ($this->options['link_format']) {
      case 'plain':
        return $image;
      break;
      case 'amazon':
        if ($detailpageurl) {
          // $link = Link::fromTextAndUrl('Amazon Link', Url::fromUri($detailpageurl));
          $link = Link::fromTextAndUrl($image, Url::fromUri($detailpageurl));
          $project_link = $link->toRenderable();
          return $project_link;
        }
        else {
          return $image;
        }
        break;
    }
  }

}
