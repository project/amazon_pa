<?php

namespace Drupal\asin\Plugin\Field\FieldType;

use Drupal\amazon_pa\Utils\AmazonPaUtils;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'asin' field type.
 *
 * @FieldType(
 *   id = "asin",
 *   label = @Translation("Amazon asin field"),
 *   module = "asin",
 *   description = @Translation("Amazon ASIN field"),
 *   default_widget = "asin_text",
 *   default_formatter = "asin_plain"
 * )
 */
class AmazonField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $locale = $field_definition->getSetting('locale');

    return [
      'columns' => [
        'asin' => [
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('asin')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['asin'] = DataDefinition::create('string')->setLabel(t('ASIN value'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'locale' => 'US',
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   *
   * Amazon locale settings on field settings
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $utils = new AmazonPaUtils();
    $cache = $utils->amazon_pa_data_cache(FALSE);
    $config = \Drupal::config('amazon_pa.settings');

    $locale_options = ['' => '-- Select --'];
    foreach ($cache['locales'] as $locale => $data) {
      if ($config->get('amazon_locale_' . $locale . '_associate_id')) {
        $locale_options[$locale] = $data['name'];
      }
    }

    $element['locale'] = [
      '#title' => t('Amazon Locale'),
      '#type' => 'select',
      '#options' => $locale_options,
      '#default_value' => $this->getSetting('locale'),
      '#description' => t('Madatory locale'),
      '#required' => TRUE,
    ];

    return $element;

  }

}
