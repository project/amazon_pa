<?php

namespace Drupal\asin\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'asin_text' widget.
 *
 * @FieldWidget(
 *   id = "asin_text",
 *   module = "asin",
 *   label = @Translation("ASIN as simple text"),
 *   field_types = {
 *     "asin"
 *   }
 * )
 */
class AmazonFieldWidget extends WidgetBase {

  //    /**
  //     * {@inheritdoc}
  //     */
  //    public function settingsSummary() {
  //      $summary = [];
  //      $summary[] = $this->t('Displays the random string.');
  //      return $summary;
  //    }
  //
  //    /**
  //     * {@inheritdoc}
  //     */
  //    public static function defaultSettings() {
  //      return [
  //          // Declare a setting named 'text_length', with
  //          // a default value of 'short'
  //          'text_length' => 'short',
  //        ] + parent::defaultSettings();
  //    }
  //
  //
  //    /**
  //     * {@inheritdoc}
  //     */
  //    public function settingsForm(array $form, FormStateInterface $form_state) {
  //      $form['text_length'] = [
  //        '#title' => $this->t('Text length'),
  //        '#type' => 'select',
  //        '#options' => [
  //          'short' => $this->t('Short'),
  //          'long' => $this->t('Long'),
  //        ],
  //        '#default_value' => $this->getSetting('text_length'),
  //      ];
  //
  //      return $form;
  //    }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->asin) ? $items[$delta]->asin : '';
    $locale =  $this->getFieldSetting('locale');

    $element['asin'] = [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#size' => 30,
      '#maxlength' => 15,
      '#element_validate' => [
        [$this, 'validate'],
      ],
    ];

    //check if we have data in the database and if, display it
    $asin_data = amazon_pa_item_lookup_from_db($value, $locale);;
    if(!empty($asin_data)){
      $element['asin_data'] = [
        '#markup' => $asin_data[$value]['title'],
      ];
    }

    return $element;
  }

  /**
   * Validate the asin field.
   *
   * @param                                      $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validate($element, FormStateInterface $form_state) {
    $asin = $element['#value'];
    //locale of the field
    $locale =  $this->getFieldSetting('locale');

    // empty field value
    if (strlen($asin) === 0) {
      $form_state->setValueForElement($element, '');
    }
    else {
      $asins[] = $asin;

      $update_one_edit_enabled = \Drupal::config('amazon_pa.settings')->get(
        'update.amazon_update_on_node_edit'
      );

      // query api directly if set in the admin settings
      if ($update_one_edit_enabled == 1) {
        $asin_data = amazon_pa_item_lookup_from_web($asins, $locale);
      }
      // use database cache
      else {
        $asin_data = amazon_pa_item_lookup($asins, $locale);
      }

      // invalid asin! amazon api gives us no data
      if (empty($asin_data)) {
        $form_state->setError($element, t("This is not a valid Amazon ASIN"));
      }
    }
  }

}
