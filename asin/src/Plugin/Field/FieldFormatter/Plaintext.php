<?php

namespace Drupal\asin\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'asin_plain' formatter.
 *
 * @FieldFormatter(
 *   id = "asin_plain",
 *   module = "asin",
 *   label = @Translation("Show ASIN as text"),
 *   field_types = {
 *     "asin"
 *   }
 * )
 */
class Plaintext extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $value) {
      $asin = trim($value->asin);

      if (!empty($asin)) {

        // plaintext asin output
        $element[$delta] = [
            '#type' => 'markup',
            '#markup' => $asin,
          ];
      }
    }
    return $element;
  }

}
