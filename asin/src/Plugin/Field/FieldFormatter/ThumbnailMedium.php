<?php

namespace Drupal\asin\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'asin_thumbnail_medium' formatter.
 *
 * @FieldFormatter(
 *   id = "asin_thumbnail_medium",
 *   module = "asin",
 *   label = @Translation("Product Thumbnail MEDIUM"),
 *   field_types = {
 *     "asin"
 *   }
 * )
 */
class ThumbnailMedium extends FormatterBase {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $config = \Drupal::config('amazon_pa.settings');

    $field_settings = $this->getFieldSettings();

    if ($field_settings['locale']) {
      $locale = $field_settings['locale'];
    }
    else {
      $locale = $config->get('amazon_default_locale');
    }

    foreach ($items as $delta => $value) {

      $asin = trim($value->asin);

      if (!empty($asin)) {
        // Lookup :: Search the amazon_item table or request Amazon API information
        $lookup = amazon_pa_item_lookup($asin, FALSE, $locale);

        // If :: Check to see if Amazon Product was returned succesfully
        if (!empty($lookup) && $item = $lookup[$asin]) {

          // detailed or nor, for fine grained output url only, title only..
          $theme_function = 'amazon_item_thumbnail_medium';

          $elements = [
            '#theme' => $theme_function,
            '#item' => $item,
          ];

          $element[$delta] = $elements;

        }

      }
    }
    return $element;
  }

}
