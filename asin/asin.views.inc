<?php

/**
 * @file
 * Include file for defining views handlers and data.
 *
 * https://api.drupal.org/api/drupal/core%21modules%21views%21views.api.php/function/hook_views_data/9.0.x
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * implementation of hook_views_data()
 *
 * @return mixed
 */
function asin_views_data() {

  /**
   * IMAGE DATA from amazon_item table
   */

  $views_data['amazon_item'] = [];
  $views_data['amazon_item']['table'] = [
    'group' => t('Amazon Item data'),
    // This is a base table, so we declare it to Views.
    'base'  => [
      // The primary key of this table. Views utilizes it for various internal
      // tasks. In fact, Views module requires all base tables to have a single-
      // column primary key.
      'field' => 'asin',
      'title' => t('Amazon Item'),
      'help'  => t('Base table for amazon products containing asins and product data'),
    ],
  ];

  // now add all fields avaliable in the amazon_item table
  // standard handlers sort/filter/argumenmt: string, numeric, boolean, date

  // As we describe the Views field 'asin' that belongs to 'amazon_item' table, we
  // nest it respectively within $views_data array.
  $views_data['amazon_item']['asin'] = [
    // Human friendly name of your Views field.
    'title'    => t('ASIN'),
    // A little more verbose description of your Views field.
    'help'     => t('ASIN of product'),
    // Apart from field we could also describe sorting, filtering on this
    // column, but for now let's just stick to the 'field' part.
    'field'    => [
      // In the 'id' key we place the name of Views field plugin that is
      // responsible for operating our 'label' column. The id "standard"
      // corresponds to \Drupal\views\Plugin\views\field\Standard class. It is
      // an extremely simplistic straightforward field plugin that appends the
      // column into SELECT query (see ::query() method) and then displays the
      // fetched column as field output (see ::render() method).
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $views_data['amazon_item']['title'] = [
    'title'    => t('Title'),
    'help'     => t('Title of product'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];
  $views_data['amazon_item']['detailpageurl'] = [
    'title'    => t('Link to the productpage'),
    'help'     => t('Link to the product detail page with affiliate token inside the url'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];
  $views_data['amazon_item']['salesrank'] = [
    'title'    => t('Sales rank'),
    'help'     => t('The current sales ranking of the product'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'number',
    ],
    'argument' => [
      'id' => 'number',
    ],
  ];
  $views_data['amazon_item']['manufacturer'] = [
    'title'    => t('Manufacturer'),
    'help'     => t('The manufacturer of the product.'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];
  $views_data['amazon_item']['mpn'] = [
    'title'    => t('MPN - part number'),
    'help'     => t("The Manufacturer's own part number for the product."),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];
  $views_data['amazon_item']['binding'] = [
    'title'    => t('Binding'),
    'help'     => t('Category like electronics.'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];
  $views_data['amazon_item']['productgroup'] = [
    'title'    => t('Product group'),
    'help'     => t('The Amazon grouping the product is categorized in.'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];
  $views_data['amazon_item']['producttypename'] = [
    'title'    => t('Product type name'),
    'help'     => t('The Amazon internal product-type code for the product.'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];
  $views_data['amazon_item']['releasedate'] = [
    'title'    => t('Release date'),
    'help'     => t('The release date of the product.'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'date',
    ],
    'argument' => [
      'id' => 'date',
    ],
  ];
  $views_data['amazon_item']['listpriceamount'] = [
    'title'    => t('List price (numeric)'),
    'help'     => t('The current sales ranking of the product on Amazon.'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'number',
    ],
    'argument' => [
      'id' => 'number',
    ],
  ];
  $views_data['amazon_item']['listpriceformattedprice'] = [
    'title'    => t('List price (formatted)'),
    'help'     => t('The current list price of the item.'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];
  $views_data['amazon_item']['lowestpriceamount'] = [
    'title'    => t('Lowest price (numeric)'),
    'help'     => t('The current lowest price offered on Amazon.'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'number',
    ],
    'argument' => [
      'id' => 'number',
    ],
  ];
  $views_data['amazon_item']['lowestpriceformattedprice'] = [
    'title'    => t('Lowest price (formatted)'),
    'help'     => t('The lowest available price.'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];
  $views_data['amazon_item']['amazonpriceamount'] = [
    'title'    => t('Amazon Price (numeric)'),
    'help'     => t("Amazon's current price for the item"),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'number',
    ],
    'argument' => [
      'id' => 'number',
    ],
  ];
  $views_data['amazon_item']['amazonpriceformattedprice'] = [
    'title'    => t('Amazon price (formatted)'),
    'help'     => t("Amazon's current price for the item."),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'string',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];
  $views_data['amazon_item']['invalid_asin'] = [
    'title'    => t('Invalid ASIN'),
    'help'     => t('If nonzero, the ASIN is invalid or discontinued by Amazon'),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'number',
    ],
    'argument' => [
      'id' => 'number',
    ],
  ];

  $views_data['amazon_item']['isbuyboxwinner'] = [
    'title'    => t('Is buy box winner'),
    'help'     => t("Amazon's price shown in the buying box"),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'string',
    ],
    'filter'   => [
      'id' => 'number',
    ],
    'argument' => [
      'id' => 'number',
    ],
  ];

  $views_data['amazon_item']['savingspercentage'] = [
    'title'    => t('Savings percentage(formatted)'),
    'help'     => t("The amount of money and percentage you save."),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $views_data['amazon_item']['savingspercentage_unf'] = [
    'title'    => t('Savings percentage(unformatted int)'),
    'help'     => t("The amount of money and percentage you save."),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'standard',
    ],
    'filter'   => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
  ];

  $views_data['amazon_item']['savingsbasis'] = [
    'title'    => t('The basis price'),
    'help'     => t("The original price where savings get subtracted from"),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'string',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $views_data['amazon_item']['pricetype'] = [
    'title'    => t('Price type Constant'),
    'help'     => t("Not intended for display. Used internaly to show what price it is."),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'string',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $views_data['amazon_item']['pricetypelabel'] = [
    'title'    => t('Label for the price type'),
    'help'     => t("What kind of price is it?"),
    'field'    => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'string',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];


  /**
   * IMAGE DATA from amazon_item_image table
   */

  // image table join
  $views_data['amazon_item_image']['table'] = [
    'title' => t('Amazon Item Image'),
    'group' => t('Amazon Item Data'),
    'join'  => [
      'amazon_item' => [
        'left_field' => 'asin',
        'field'      => 'asin',
      ],
    ],
  ];

  // image field with custom handler
  $views_data['amazon_item_image']['url'] = [
    'title' => t('Amazon Product image'),
    'help'  => t("An image of the Amazon product."),
    'field' => [
      'id' => 'views_handler_field_amazon_image',
    ],
  ];

  /**
   * PARTICIPANT DATA from amazon_item_participant table
   */

  // table join
  $views_data['amazon_item_participant']['table'] = [
    'title' => t('Amazon Item Participant'),
    'group' => t('Amazon Item Data'),
    'join'  => [
      'amazon_item' => [
        'left_field' => 'asin',
        'field'      => 'asin',
      ],
    ],
  ];

  // participant
  $views_data['amazon_item_participant']['participant'] = [
    'title' => t('Participant name'),
    'help'  => t("The name of an individual who participated in the creation of a product."),
    'field' => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'string',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  // participant type
  $views_data['amazon_item_participant']['type'] = [
    'title' => t('Participant role'),
    'help'  => t("The role the participant had in creating the product (author, artist, etc)."),
    'field' => [
      'id' => 'standard',
    ],
    'sort'     => [
      'id' => 'string',
    ],
    'filter'   => [
      'id' => 'string',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  // @todo participant all handler how? what is the field id and array index then? there is no field participant_all?
  // do we need to add it and then fill it in teh custom handler?

  return $views_data;
}

/**
 * Implements hook_field_views_data().
 *
 * Through this hook we're given the chance to change the views schema
 * data for the asin field. The primary thing to be done is to add a join
 * on the ASIN type to the amazon_item views base stuff.
 * //https://api.drupal.org/api/drupal/core!modules!views!views.api.php/function/hook_field_views_data/8.8.x
 *
 * @param \Drupal\field\FieldStorageConfigInterface $field_storage
 *
 * @return array
 */
function asin_field_views_data(FieldStorageConfigInterface $field_storage) {
  $data = views_field_default_views_data($field_storage);
  foreach ($data as $table_name => $table_data) {

    foreach ($table_data as $field_name => $field_data) {

      // Check for fieldapi value fields.
      if (isset($field_data['filter']['field_name'])) {
        $data[$table_name][$field_name]['relationship'] = [
          // 'handler' => 'views_handler_relationship',
          'id'         => 'standard',
          'base'       => 'amazon_item',
          'base_field' => 'asin',
          'label'      => t('ASIN', ['@field_name' => $field_storage->getName()]),
        ];
      }
    }
  }

  return $data;
}
