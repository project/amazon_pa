<?php

namespace Drupal\Tests\asin\Kernel\Field;

use Drupal\Core\Database\Database;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests the ASIN formatters functionality.
 *
 * @group text
 */
class AsinFormatterTest extends EntityKernelTestBase {

  /**
   * The entity type used in this test.
   *
   * @var string
   */
  protected $entityType = 'entity_test';

  /**
   * The machine name of the asin field to test with.
   *
   * @var string
   */
  protected $fieldName = 'test_field_asin';

  /**
   * A valid ASIN to test with.
   *
   * @var string
   */
  protected $asin = 'B01KQPV71Y';

  /**
   * The bundle used in this test.
   *
   * @var string
   */
  protected $bundle = 'entity_test';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['asin', 'amazon_pa'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $amazon_pa_tables = [
      'amazon_item',
      'amazon_item_image',
      'amazon_item_image_gallery',
      'amazon_item_participant',
    ];
    $this->installSchema('amazon_pa', $amazon_pa_tables);
    // Populate the test {amazon_item} table with sample data.
    // @todo Generalize this into a system with test fixtures.
    $data = [
      'asin' => $this->asin,
      'locale' => 'US',
      'title' => 'Salaam Axe',
      'detailpageurl' => 'https://www.amazon.com/dp/B01KQPV71Y',
      'manufacturer' => 'CD Baby',
      'binding' => 'Audio CD',
      'releasedate' => '2016-08-02T00:00:01Z',
    ];
    $connection = Database::getConnection();
    $connection->insert('amazon_item')
      ->fields($data)
      ->execute();

    FieldStorageConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => $this->entityType,
      'type' => 'asin',
      'settings' => [],
    ])->save();
    FieldConfig::create([
      'entity_type' => $this->entityType,
      'bundle' => $this->bundle,
      'field_name' => $this->fieldName,
      'label' => 'ASIN Field label',
    ])->save();
  }

  /**
   * Tests all text field formatters.
   * @doesNotPerformAssertions
   */
  public function testFormatters() {
    $formatters = [
      'asin_details' => 'checkDetails',
      //'asin_gallery_large' => 'checkGalleryLarge',
      //'asin_gallery_medium' => 'checkGalleryMedium',
      //'asin_gallery_medium_details' => 'checkGalleryMediumDetails',
      //'asin_gallery_small' => 'checkGallerySmall',
      // Broken because of $style weirdness.
      //'asin_detailpageurl' => 'checkDetailPageUrl',
      'asin_plain' => 'checkPlain',
      //'asin_thumbnail_large' => 'checkThumbnailLarge',
      //'asin_thumbnail_medium' => 'checkThumbnailMedium',
      //'asin_thumbnail_medium_title' => 'checkThumbnailMediumTitle',
      //'asin_thumbnail_small' => 'checkThumbnailSmall',
    ];

    // Create the entity to be referenced.
    $entity = $this->container->get('entity_type.manager')
      ->getStorage($this->entityType)
      ->create(['name' => $this->randomMachineName()]);
    $entity->get($this->fieldName)->set(0, $this->asin);
    $entity->save();

    foreach ($formatters as $formatter_id => $checkMethod) {
      // Verify the text field formatter's render array.
      $build = $entity->get($this->fieldName)->view(['type' => $formatter_id]);
      \Drupal::service('renderer')->renderRoot($build[0]);
      $this->{$checkMethod}($build);
    }
  }

  /**
   * Checks output from the 'asin_details' formatter.
   *
   * @param array $build
   *   The render array that the formatter generates.
   */
  protected function checkDetails(array $build) {
    $this->assertStringContainsString('Salaam Axe', $build[0]['#markup']);
    $this->assertStringContainsString('https://www.amazon.com/dp/B01KQPV71Y', $build[0]['#markup']);
    $this->assertStringContainsString('Manufacturer', $build[0]['#markup']);
    $this->assertStringContainsString('Price', $build[0]['#markup']);
    $this->assertStringContainsString('Part Number', $build[0]['#markup']);
  }

  /**
   * Checks output from the 'asin_detailpageurl' formatter.
   *
   * @param array $build
   *   The render array that the formatter generates.
   */
  protected function checkDetailPageUrl(array $build) {
    // @todo
  }

  /**
   * Checks output from the 'asin_details' formatter.
   *
   * @param array $build
   *   The render array that the formatter generates.
   */
  protected function checkPlain(array $build) {
    $this->assertEquals($this->asin, $build[0]['#markup']);
  }

}
