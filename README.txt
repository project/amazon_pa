Amazon Product Advertising (Affiliate)

Recommendations
----------------
The module can not ships a view for the amazon datatable with all its data! Because everyone names there is fields differently. The following is recommended. I recommend creating a custom view(normal content) with a RELATIONSHIP to the amazon_field (you created that on a node). There you can also list invalid asin data for fields ONLY!


Templates
-------------
Do not add the break and <p> filter below the amazon filter.
There is a filter list on /admin/config/content/formats/manage/full_html?destination=/admin/config/content/formats
There the amazon filter should be ABOVE the mentioned <p> and <br> filter. Otherwise you get <p> tags and <br> in your templates and you may be confused about these killing your design.


Views
----------
Most pages add views listings of nodes with small teasers adding a field using a textfilter. The body field is such a field and in that field amazon tokens can be used.
If you add the body field, you should select the "advanced" option in that fieled and select "filter: none"
Why you may ask? That is because if you do not do that, the amazon filter will be run on all that teaser texts even there is only little text shown. Meaning it will process all complete body fields!
This is because without the advanted settings it is a body field and the filter will be run, thats how it works.
